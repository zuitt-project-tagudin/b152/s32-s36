const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;


router.post('/', verify, verifyAdmin, courseController.addCourse);

router.get('/', courseController.getAllCourses);

router.get('/getSingleCourse/:id', courseController.getSingleCourse);

router.put('/:id', verify, verifyAdmin, courseController.updateCourse);

router.put('/archive/:id', verify, verifyAdmin, courseController.archiveCourse);

router.put('/activate/:id', verify, verifyAdmin, courseController.activateCourse);

router.get('/getActiveCourses', courseController.getActiveCourses);

router.get('/getInactiveCourses', verify, verifyAdmin, courseController.getInactiveCourses);

router.post('/findCourseName', courseController.findCourseName);

router.post('/findCoursePrice', courseController.findCoursePrice);

router.get('/getEnrollees/:id', verify, verifyAdmin, courseController.getEnrollees);


module.exports = router;