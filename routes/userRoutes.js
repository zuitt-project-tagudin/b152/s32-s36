const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;


router.post("/", userControllers.registerUser);

router.get("/", userControllers.getAllUsers);

router.post("/login", userControllers.loginUser);

router.get("/getUserDetails", verify, userControllers.getUserDetails);

router.post("/checkEmailExists", userControllers.checkEmail);

router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

router.post('/enroll', verify, userControllers.enroll);

router.get("/getEnrollments", verify, userControllers.getEnrollments);


module.exports = router;