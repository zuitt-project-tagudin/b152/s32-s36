const express = require ("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://mongoadmin:admin@cluster0.sao0e.mongodb.net/bookingAPI152?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("Connected to MongoDB"));

app.use(express.json());

const userRoutes = require("./routes/userRoutes");
app.use('/users', userRoutes);

const courseRoutes = require("./routes/courseRoutes");
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`running on ${port}`));