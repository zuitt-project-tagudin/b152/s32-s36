const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt"); //layer of security for passwords
const auth = require("../auth");


module.exports.registerUser = (req, res) => {

	console.log(req.body);


	const hashedPW = bcrypt.hashSync(req.body.password, 10);
//	console.log(hashedPW);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))

}

module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser =(req, res) => {

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if (foundUser === null) {
			return res.send("no user found. sign up")
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			
			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("incorrect password")
			}
		}


	})
	.catch(error => res.send(error));
}	

module.exports.getUserDetails = (req, res) => {

	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));

	//res.send("testing for verify")
}

module.exports.checkEmail = (req, res) => {

	
	User.findOne({email: req.body.email})
	.then(result => {

		if (result === null) {
			res.send("Email is available")
		} else {
			res.send("Email is already registered")
		}

	})
	.catch(error => res.send(error))


}

module.exports.updateUserDetails = (req, res) => {


	console.log(req.user);

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateAdmin = (req, res) => {

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.enroll = async (req, res) => {

	if (req.user.isAdmin) {
		return res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message)
	})

	console.log(isUserUpdated);

	if (isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	} 

	let isCourseUpdated = await Course.findById(req.body.courseId). then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollments.push(enrollee);

		return course.save().then(course => true).catch(err => err.message)
	})

	console.log(isCourseUpdated);

	if (isCourseUpdated !== true) {
		return res.send({message: isCourseUpdated});
	}

	if (isUserUpdated && isCourseUpdated) {
		return res.send({message: "Enrolled Successfully"})
	}
}

module.exports.getEnrollments = (req, res) => {

	console.log(req.user);
	User.findById(req.user.id)
	.then(foundUser => res.send(foundUser.enrollments))
	.catch(error => res.send(error));
}

