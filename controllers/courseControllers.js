const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {


	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));

}

module.exports.getAllCourses = (req, res) => {
	
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getSingleCourse = (req, res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
	
}

module.exports.updateCourse = (req, res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));

}

module.exports.archiveCourse = (req, res) => {

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));

}

module.exports.activateCourse = (req, res) => {

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));

}

module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

module.exports.findCourseName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options: "i"}})
	.then(result => {

		if (result.length === 0) {
			return res.send("no course found")
		} else {
			res.send(result)
		}
	})
	.catch(error => res.send(error));
}

module.exports.findCoursePrice = (req, res) => {
	
	Course.find({price: req.body.price})
	.then(result => {

		if (result.length === 0) {
			return res.send("no course found")
		} else {
			res.send(result)
		}
	})
	.catch(error => res.send(error));
}

module.exports.getEnrollees = (req, res) => {
	
	Course.findById(req.params.id)
	.then(foundCourse => res.send(foundCourse.enrollments))
	.catch(error => res.send(error));
}