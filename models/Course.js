const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "name is required"]
	},
	description: {
		type: String,
		required: [true, "description is required"]
	},
	price: {
		type: Number,
		required: [true, "price"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollments: [

		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]


})

module.exports = mongoose.model("Course", courseSchema);